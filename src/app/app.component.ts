import { Component, OnInit } from '@angular/core';
import { FirstTestDto } from '@common/dtos';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
}
