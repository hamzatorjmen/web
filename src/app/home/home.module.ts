import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HomeComponent } from './home.component';
import { HomeService } from './home.service';

@NgModule({
    imports: [
        HttpClientModule,
        BrowserModule
    ],
    declarations: [
        HomeComponent
    ],
    exports: [HomeComponent],
    providers: [HomeService]
})
export class HomeModule {
}
