import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FirstTestDto } from '@common/dtos';

@Injectable()
export class HomeService {
constructor(private http: HttpClient) {}
    public async getFirstTest(): Promise<FirstTestDto[]> {
        return this.http.get<FirstTestDto[]>('https://g-news-marketing.herokuapp.com/gnews?keyword=tunisair').toPromise();
    }

}
