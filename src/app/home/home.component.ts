import { Component, OnInit } from '@angular/core';
import { FirstTestDto } from '@common/dtos';
import { HomeService } from './home.service';

@Component({
    selector: 'app-home',
    templateUrl : './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    constructor(private appService: HomeService) {}
  public data: FirstTestDto[] = [];
  public inputText = '';

  public async ngOnInit(): Promise<void> {
    this.data = await this.appService.getFirstTest();
    console.log('hahahaa');
    console.log(this.data);
  }
}

